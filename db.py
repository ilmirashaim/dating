'''
 Contains classes to map the database tables to the python classes
'''
from sqlalchemy import Column, ForeignKey, Integer,\
   String, Boolean, Date, UniqueConstraint, PrimaryKeyConstraint,\
   Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import ClauseElement
dbName = 'mysql://dating:dating@localhost/dating?charset=utf8'

Base = declarative_base()
engine = create_engine(dbName)
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
dbSession = DBSession()


class User(Base):
   __tablename__ = 'user'
   __table_args__ = {'mysql_engine':'InnoDB'}

   id = Column(Integer, primary_key=True, autoincrement=True)
   uid = Column(Integer, index=True, unique=True)
   first_name = Column(String(250, convert_unicode=True), nullable=False)
   last_name = Column(String(250, convert_unicode=True), nullable=False)
   sex = Column(Boolean, nullable=True)
   bdate = Column(Date, nullable=True)
   last_seen_hours = Column(Integer, nullable=True)
   last_seen_days = Column(Integer, nullable=True)
   zodiac = Column(String(250, convert_unicode=True), nullable=True)

   city = Column(Integer, nullable=True)
   country = Column(Integer, nullable=True)
   photo_100 = Column(String(250, convert_unicode=True), nullable=True)

   deactivated = Column(Boolean, nullable=False, default=False)
   verified = Column(Boolean, nullable=False, default=False)
   wall_comments = Column(Boolean, nullable=False, default=False)

   occupation_type = Column(String(250, convert_unicode=True), nullable=True)
   occupation_id = Column(Integer, nullable=True)

   home_town = Column(String(250, convert_unicode=True), nullable=True)

   personal_political = Column(Integer, nullable=True)
   personal_religion = Column(String(250, convert_unicode=True), nullable=True)
   personal_life_main = Column(Integer, nullable=True)
   personal_smoking = Column(Integer, nullable=True)
   personal_alcohol = Column(Integer, nullable=True)
   personal_people_main = Column(Integer, nullable=True)
   
   counters_photos = Column(Integer, nullable=True)
   counters_audios = Column(Integer, nullable=True)
   counters_videos = Column(Integer, nullable=True)
   counters_notes = Column(Integer, nullable=True)
   counters_groups = Column(Integer, nullable=True)
   counters_followers = Column(Integer, nullable=True)
   counters_pages = Column(Integer, nullable=True)

   music = Column(Text(convert_unicode=True), nullable=True)
   interests = Column(Text(convert_unicode=True), nullable=True)
   activities = Column(Text(convert_unicode=True), nullable=True)
   tv = Column(Text(convert_unicode=True), nullable=True)
   games = Column(Text(convert_unicode=True), nullable=True)
   books = Column(Text(convert_unicode=True), nullable=True)
   movies = Column(Text(convert_unicode=True), nullable=True)

   def toDict(user):
      res = {x:user.__dict__[x] for x in user.__dict__ if x!='_sa_instance_state'}
      return res

class Couple(Base):
   __tablename__ = 'couple'
   __table_args__ = {'mysql_engine':'InnoDB'}
   
   id = Column(Integer, primary_key=True, autoincrement=True)
   guy_id = Column(Integer, ForeignKey('user.uid'))
   girl_id = Column(Integer, ForeignKey('user.uid'))
   guy = relationship(User, foreign_keys=[guy_id])
   girl = relationship(User, foreign_keys=[girl_id])
   guy_relation = Column(Integer, nullable=True)
   girl_relation = Column(Integer, nullable=True)


class UserCareer(Base):
   __tablename__ = 'user_career'
   __table_args__ = {'mysql_engine':'InnoDB'}

   id = Column(Integer, primary_key=True)
   city = Column(Integer, nullable=True)
   country = Column(Integer, nullable=True)
   position = Column(String(250, convert_unicode=True), nullable=True)

   user_id = Column(Integer, ForeignKey('user.uid'))
   user = relationship(User)

class University(Base):
   __tablename__ = 'university'
   __table_args__ = {'mysql_engine':'InnoDB'}

   id = Column(Integer, primary_key=True, autoincrement=True)
   city = Column(Integer, nullable=True)
   country = Column(Integer, nullable=True)
   name = Column(String(250, convert_unicode=True), nullable=True)
   faculty_name = Column(String(250, convert_unicode=True), nullable=True)

   university_id = Column(Integer, nullable=False)
   faculty_id = Column(Integer, nullable=True)

   UniqueConstraint('university_id', 'faculty_id')

class UserUniversity(Base):
   __tablename__ = 'user_university'
   __table_args__ = {'mysql_engine':'InnoDB'}

   user_id = Column(Integer, ForeignKey('user.uid'), primary_key=True)
   user = relationship(User)

   university_id = Column(Integer, ForeignKey('university.id'), primary_key=True)
   university = relationship(University)

   PrimaryKeyConstraint('user_id', 'university_id')

class School(Base):
   __tablename__ = 'school'
   __table_args__ = {'mysql_engine':'InnoDB'}

   id = Column(Integer, primary_key=True, autoincrement=True)
   city = Column(Integer, nullable=True)
   country = Column(Integer, nullable=True)
   name = Column(String(250, convert_unicode=True), nullable=True)
   type = Column(Integer, nullable=True)
   school_id = Column(Integer, nullable=False)

   UniqueConstraint('school_id')

class UserSchool(Base):
   __tablename__ = 'user_school'
   __table_args__ = {'mysql_engine':'InnoDB'}

   id = Column(Integer, primary_key=True, autoincrement=True)
   user_id = Column(Integer, ForeignKey('user.uid'), primary_key=True)
   user = relationship(User)

   school_id = Column(Integer, ForeignKey('school.id'), primary_key=True)
   school = relationship(School)
   PrimaryKeyConstraint('user_id', 'school_id')

def get_or_create(session, model, defaults=None, **kwargs):
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance, False
    else:
        params = dict((k, v) for k, v in kwargs.iteritems() if not isinstance(v, ClauseElement))
        params.update(defaults or {})
        instance = model(**params)
        session.add(instance)
        return instance, True


def create_scheme():

   engine.execute('set foreign_key_checks=0')
   Base.metadata.drop_all(engine)
   Base.metadata.create_all(engine)
   engine.execute('set foreign_key_checks=1')


if __name__ == "__main__":
   create_scheme()