'''
Contains method to collect all needful data from API
'''

from apiToDb import collectAndCreateFriends, requestFriendsWithRepeat
from sqlalchemy.exc import SQLAlchemyError
import logging
import traceback
import sys
from db import dbSession

if __name__ == '__main__':
   id = "3853209"
   access_token = "" #put something here
   session = {'user_id': id, 'access_token': access_token}
# to get token:
# https://oauth.vk.com/authorize?client_id=5652867&display=page&redirect_uri=http://127.0.0.1&response_type=token

   friends = requestFriendsWithRepeat(int(id), session, ['uid'])

   friends = collectAndCreateFriends(friends, dbSession, session)
   uids = [f.uid for f in friends]
   friendsFriends_uids = []
   for uid in uids:
      print "collecting friends for %d" % uid
      try:
         friends = requestFriendsWithRepeat(uid, session, ['uid'])
         res = collectAndCreateFriends(friends, dbSession, session)
         friendsFriends_uids.extend([f.uid for f in res])
      except SQLAlchemyError as e:
         print e.message
         logging.error(traceback.format_exc())
         dbSession.rollback()
      except Exception as e:
         logging.error(traceback.format_exc())
         print('error while collecting friends from user %d'% uid)
         print "Unexpected error:", sys.exc_info()[0]
         dbSession.rollback()


   print('STARTING FRIENDS FRIENDS')
   for uid in friendsFriends_uids:
      try:
         friends = requestFriendsWithRepeat(uid, session, ['uid'])
         res = collectAndCreateFriends(friends, dbSession, session)
      except SQLAlchemyError as e:
         print e.message
         logging.error(traceback.format_exc())
         dbSession.rollback()
      except Exception as e:
         logging.error(traceback.format_exc())
         print('error while collecting friends from user %d'% uid)
         print "Unexpected error:", sys.exc_info()[0]
         dbSession.rollback()