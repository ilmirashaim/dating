'''
This file contains methods which are used to collect recommendations

'''
from operator import itemgetter
import codecs
import sys

from sklearn.externals import joblib

from api import requestFriendsWithRepeat
from build import modelFilename
from build import compareUsers
from db import dbSession, User
from apiToDb import collectAndCreate
from apiToDb import collectAndCreateFriends

cache = {}

def collectResults(users, predictions, predictions_proba):
   nUsers = len(users)
   res = []
   for i in xrange(nUsers):
      prob = predictions_proba[i][1]
      pred = predictions[i]
      res.append({'id': users[i].uid,
          'name': (users[i].first_name.capitalize() + ' ' + users[i].last_name.capitalize()),
          'proba': round(prob, 3),
          'pred': pred,
           'photo': users[i].photo_100})
   return res

def getRecommendations(user_id, httpSess):
   if user_id in cache:
      return cache[user_id]

   user = dbSession.query(User).filter_by(uid=user_id).first()
   if not user:
      x, user, created = collectAndCreate(user_id, dbSession, httpSess)

   friends = requestFriendsWithRepeat(user_id, httpSess, ['photo_100', 'sex'])
   if not friends:
      return []
   friends = [f for f in friends if f['sex']-1 != user.sex]
   uids = [f['uid'] for f in friends]

   friends_users = dbSession.query(User).filter(User.uid.in_(uids)).all()
   if not friends_users:
      return []
   collected = [f.uid for f in friends_users]
   notCollected = [{'uid':u} for u in uids if u not in collected]
   friends_users_rest = collectAndCreateFriends(notCollected, dbSession, httpSess)
   friends_users.extend(friends_users_rest)
   test = []

   for f in friends_users:
      if user.sex == 1:
         test.append(compareUsers(user, f))
      else:
         test.append(compareUsers(f, user))
   clf = joblib.load(modelFilename)


   predictions_proba = clf.predict_proba(test)
   predictions = clf.predict(test)

   res = collectResults(friends_users, predictions, predictions_proba)

   res = sorted(res, key=itemgetter('proba'), reverse=True)
   cache[user_id] = res
   return res

if __name__ == '__main__':
   UTF8Writer = codecs.getwriter('utf8')
   sys.stdout = UTF8Writer(sys.stdout)

   user_id = "22868231"
   res = getRecommendations(user_id, {'user_id':user_id})
   for i in res:
      strPred = u''
      for j in i:
         strPred += i[j] if type(i[j]) == unicode else str(i[j])
         strPred += u','
      print(strPred)