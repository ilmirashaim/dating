
def checkCouples(couples):
   from recommend import getRecommendations
   correct = 0
   incorrect = 0

   for couple in couples:
      if not couple.guy and not couple.girl:
         continue
      recs_guy = getRecommendations(couple.guy_id)
      if couple.girl_id in [x['id'] for x in recs_guy[:5]]:
         correct +=1
         print('correct %d' %correct)
      else:
         incorrect+=1
         print('incorrect %d' %incorrect)
      recs_girl = getRecommendations(couple.girl_id)
      if couple.guy_id in [x['id'] for x in recs_girl[:5]]:
         correct +=1
         print('correct %d' %correct)
      else:
         incorrect+=1
         print('incorrect %d' %incorrect)
   print('final correct %d' %correct)
   print('final incorrect %d' %incorrect)

