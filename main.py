'''
This file contains web server
'''

from flask import Flask, make_response, jsonify, request,\
   render_template, session, redirect
from db import User, dbSession
app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
args = {'Content-Type': 'application/json; charset=utf-8'}
app.secret_key = 'super secret key'
app.config['SESSION_TYPE'] = 'filesystem'
from apiToDb import createUserWithFriends
from recommend import getRecommendations, collectAndCreate
from api import collectToken


@app.route('/api/users/<int:user_id>/', methods=['GET'])
def users(user_id):
   user = dbSession.query(User).filter_by(uid=user_id).first()
   return make_response(jsonify(user.toDict()), 200, args)

@app.route('/')
def index():
    return render_template('index.html', is_auth = 'user_id' in session)

@app.route('/showres/', methods=['GET'])
def results():
   user_id = session['user_id']
   user = dbSession.query(User).filter_by(uid=user_id).first()
   if not user:
      x, user, created = collectAndCreate(user_id, dbSession, session)
   photo = user.photo_100
   name = user.first_name.capitalize() + " " + user.last_name.capitalize()

   return render_template("result.html", user_id=user_id, photo=photo, name=name)

@app.route('/code')
def code():
   code = request.args.get('code')
   print "code = %s" % code

   collectToken(code, session)
   return redirect('/showres')


#
# @app.route('/results/', methods=['GET'])
# def results():
#    return render_template("result.html", user_id="123")

@app.route('/api/users/', methods=['GET'])
def users_all():
   return make_response(jsonify({'error': 'no user specified'}), 200, args)

@app.route('/api/users/', methods=['PUT', 'POST'])
def users_create():
   data = request.get_json()
   if 'id' in data:
      user = createUserWithFriends(int(data['id']))
      res = user.toDict()
   else:
      res = {'error': 'no user specified'}
   return make_response(jsonify(res), 200, args)


@app.route('/api/users/<int:user_id>/recommendations', methods=['GET'])
def get_recommendations(user_id):
   recommendations = getRecommendations(user_id, session)
   # dict = [{"name":"1","proba":"123", "id":"1"}]

   return make_response(jsonify(recommendations), 200, args)

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000)
