from MySQLdb.constants.FIELD_TYPE import NULL

LIMIT = 3

def filter(userlist):
    newUserlist = []
    for user in userlist:
        count = 0
        for element in user:
            if (element is None): count = count + 1
        if count <= LIMIT: newUserlist.append(user)
    return newUserlist
