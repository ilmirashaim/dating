from build_classifier import categoricalFeatures
from build_classifier import numericFeatures
from database_setup import UserUniversity, \
   UserSchool, UserCareer, Couple, User

def processUserById(user_id, session):
   user = session.query(User).filter_by(uid=user_id).first()
   if user is None:
      return None
   return processUser(user, session)

def processUser(user, session):
   values = []
   for x in numericFeatures:
      if x == 'sex':
         values.append(float(user.sex+1))
         continue
      if x == 'year':
         year = user.bdate.year if user.bdate else None
         values.append(float(year))
         continue
      x = x.replace('.','_')
      if x == 'counters_videos':
         values.append(None)
         continue
      val = getattr(user, x)
      values.append(float(val))

   user_univ = session.query(UserUniversity).filter_by(user_id = user.uid).first()
   univ = user_univ.university if user_univ else None
   user_school = session.query(UserSchool).filter_by(user_id = user.uid).first()
   school = user_school.school if user_school else None
   user_career = session.query(UserCareer).filter_by(user_id = user.uid).first()
   for x in categoricalFeatures:
      if x == 'city.id':
         values.append(user.city)
         continue
      if x == 'country.id':
         values.append(user.country)
         continue
      if x == 'relation':
         if user.sex == '1':
            couple = session.query(Couple).filter_by(guy_id = user.uid).first()
            relation = couple.guy_relation if (couple and couple.guy_relation) else None
         else:
            couple = session.query(Couple).filter_by(guy_id = user.uid).first()
            relation = couple.girl_relation if (couple and couple.girl_relation) else None
         values.append(relation)
         continue
      if x == 'site':
         values.append(None)
         continue
      if x.startswith('university'):
         if user_univ and univ:
            if x == 'university.id':
               values.append(univ.university_id)
               continue
            if x == 'university.faculty':
               values.append(univ.faculty_id)
               continue
            if x == 'university.faculty_name':
               values.append(univ.faculty_name)
               continue
         else:
            values.append(None)
            continue

      if x.startswith('school'):
         if user_school and school:
            if x == 'school.type':
               values.append(school.type)
               continue
            if x == 'school.id':
               values.append(school.school_id)
               continue
         else:
            values.append(None)
            continue
      if x == 'career.position':
         values.append(user_career.position if (user_career and user_career.position) else None)
         continue
      k = x.replace('.','_')
      val = getattr(user, k)
      values.append(val)
   return values