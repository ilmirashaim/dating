'''
This file is main file for model training.
It collects list of couples from database, (one list for test and for training)
then it turns it to a list of the following structure:
   [guy1, girl1, guy2, girl2 ... guyn, girln]

Then this list is turned into list of comparison vectors:
   [vectorForCouple1, vectorForNonCouple1, vectorForCouple2, vectorForNonCouple2,
   ..., vectorForCouple_n, vectorForNonCouple_n]
   and the list of labels:
   [1,0,1,0,...,1,0]

   1 - for couple
   0 - for non-couple.

Couples are pairs of guy_i+girl_i and non-couples are pairs of guy_i+girl_i+1
Comparison vector consist of differences in features for 2 people or
default values if at least one of them is not specified.

The list of comparison vectors and labels for training is used for  training  a classifier using
n-fold cross-validation.

'''

from db import Couple, dbSession, UserCareer, UserUniversity, UserSchool
import os
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
from sklearn.externals import joblib
import sklearn.metrics as metrics
import datetime
from time import gmtime, strftime
import numpy as np


# type of the classifier, rf - random forest, svc - support vector classifier
# model = 'mlp'
# model = 'rfc'
model = 'rfc'

# file to save trained model
# modelFilename = os.path.join('model', '%s%s.pkl'%(model, strftime("%Y-%m-%d%H:%M:%S", gmtime())))
modelFilename = os.path.join('model', 'rfc_last.pkl')
# modelFilename = os.path.join('model', 'svc_50_0.01.pkl')

# default value if feature value is not specified
defNumVal = -1
defCatVal = -1

categoricalFeatures = [
    "zodiac",
    "city.id",
    "country.id",
    # "home_town",
    "occupation.id",
    "occupation.type",
    "personal.political",
    "personal.religion",
    "personal.people_main",
    "personal.life_main",
    "personal.smoking",
    "personal.alcohol",
    # "site",
    # "university.id",
    "university.faculty",
    # "university.faculty_name",
    "school.type", "school.id"]
# "career.position"]

numericFeatures = ["sex", "year",
                   "counters.videos",
                   "counters.photos",
                   "counters.audios",  # "counters.notes",
                   "counters.groups",
                   "counters.followers",
                   "counters.pages"]  # , "wall_comments"]


def main():
    # collect all available couples from database
    couples = dbSession.query(Couple).all()
    # training set: [guy1, girl1, ..., ] all the couples except first 400
    train = couplesToData(couples[400:])
    # test set: [guy1, girl1, ..., ] first 400 couples
    test = couplesToData(couples[:400])

    # comparison vectrors (X) and labels (Y)
    X, Y = generateSamples(train)
    Xtest, Ytest = generateSamples(test)

    print "Size Xtest: %d" % (len(Xtest))
    print "Size Ytest: %d" % (len(Ytest))
    print "Size X: %d" % (len(X))
    print "Size Y: %d" % (len(Y))

    # grid search for hyper parameters of the classifier
    if model == 'svc':

        param_grid = {'C': [1, 2, 3, 4, 5, 10, 50],
                      'gamma': [0.1, 0.01, 0.001, 0.0001, 0.00001, 0.000001]}
        clf = SVC(probability=True)
    elif model == 'rfc':
        param_grid = {'n_estimators': [10, 20, 100],
                      'criterion': ['gini', 'entropy'], 'max_features': ['auto', 'log2']}
        clf = RandomForestClassifier()
    elif model == 'mlp':
        param_grid = {'activation': ['relu', 'tanh'],
                      'alpha': [ 1e-04, 1e-05, 1e-06, 1e-07],
                      'learning_rate': ['constant', 'invscaling', 'adaptive'],
                      'solver': ['sgd', 'adam', 'lbfgs'],
                      'batch_size': [10,20,30],
                      'max_iter': [200, 500]}
        clf = MLPClassifier()
    gridSearchClf = GridSearchCV(clf, param_grid, scoring='accuracy', cv=3)
    gridSearchClf.fit(X, Y)


    print("Accuracy %f" % (gridSearchClf.best_score_))
    print(gridSearchClf.best_params_)
    if model == 'svc':
        C = gridSearchClf.best_params_['C']
        gamma = gridSearchClf.best_params_['gamma']
        clf = SVC(gamma=gamma, C=C, probability=True)
    elif model == 'rfc':
        n_estimators = gridSearchClf.best_params_['n_estimators']
        criterion = gridSearchClf.best_params_['criterion']
        max_features = gridSearchClf.best_params_['max_features']

        clf = RandomForestClassifier(n_estimators=n_estimators,
                                     max_features=max_features,
                                     criterion=criterion)
    elif model == 'mlp':
        activation = gridSearchClf.best_params_['activation']
        alpha = gridSearchClf.best_params_['alpha']
        learning_rate = gridSearchClf.best_params_['learning_rate']
        solver = gridSearchClf.best_params_['solver']
        batch_size = gridSearchClf.best_params_['batch_size']
        max_iter = gridSearchClf.best_params_['max_iter']
        clf = MLPClassifier(activation=activation, alpha=alpha,
                            learning_rate=learning_rate, solver=solver,
                            batch_size=batch_size, max_iter=max_iter)

    clf.fit(X, Y)

    # work only for random forest
    analyzeFeatRanks(clf)

    joblib.dump(clf, modelFilename)

    print('Test data')
    testPred = clf.predict(Xtest)
    print"confusion matrix\n %s" % str(metrics.confusion_matrix(Ytest, testPred))
    print("accuracy %f" % metrics.accuracy_score(Ytest, testPred))
    print("recall %f" % metrics.recall_score(Ytest, testPred))
    print("precision %f" % metrics.precision_score(Ytest, testPred))
    print("roc auc score %f" % metrics.roc_auc_score(Ytest, testPred))


def analyzeFeatRanks(forest):
    feats = numericFeatures + categoricalFeatures
    importances = forest.feature_importances_
    indices = np.argsort(importances)[::-1]
     # Print the feature ranking
    print("Feature ranking:")

    for f in range(len(indices)):
        print("%d. feature %d - %s (%f)" % (f + 1, indices[f], feats[indices[f]], importances[indices[f]]))
    return forest


def generateSamples(train):
    '''
    Turn a list of users into list of comparison vectors and labels
    :param train: list [guy1, girl1, ...], list of users
    :return: X - list of comparison vectors
             Y - list of labels, 1 - couple, 0 - non-couple
    '''
    X = []
    Y = []
    length = len(train)
    for i in xrange(0, 2 * (length / 2), 2):
        if i + 3 < length:
            j = i + 3
        else:
            j = i - 3

        x = compareUsers(train[i], train[i + 1])
        if passfilter(x):
            X.append(x)
            Y.append(1)

        x = compareUsers(train[i], train[j])
        if passfilter(x):
            X.append(x)
            Y.append(0)
    return X, Y


def couplesToData(couples):
    '''
    Turn a couple list to the list of users [guy1, girl1, ...]
    :param couples: couple list
    :return: list of users
    '''
    train = []
    for couple in couples:
        if not couple.girl or not couple.guy:
            # print(couple.id)
            continue
        train.append(couple.guy)
        train.append(couple.girl)
    return train


def compareUsers(guy, girl):
    '''
    For 2 users returns a comparison vector
    :param guy: user
    :param girl: user
    :return: list
    '''
    vals = [] #comparison vector

    # add comparison results for numerical features in the output vector
    for x in numericFeatures:
        x = x.replace('.', '_')
        # if x == 'counters_videos':
        #     vals.append(defNumVal)
        #     continue
        if x == 'year':
            year = datetime.date.today().year
            if (guy.bdate!= None and girl.bdate!=None):
                vals.append(float(ageCombination(year - guy.bdate.year, year - girl.bdate.year)))
            else:
                vals.append(defNumVal)
            continue
        val1 = getattr(guy, x)
        val2 = getattr(girl, x)
        if x.startswith("counters"):
            vals.append(counter_category(val1, val2, x))
            continue
        vals.append(float(abs(val1 - val2) if val1 and val2 else defNumVal))

    # retrieving the university/school info about both users
    user_univ1 = dbSession.query(UserUniversity).filter_by(user_id=guy.uid).first()
    univ1 = user_univ1.university if user_univ1 else None
    user_school1 = dbSession.query(UserSchool).filter_by(user_id=guy.uid).first()
    school1 = user_school1.school if user_school1 else None
    user_career1 = dbSession.query(UserCareer).filter_by(user_id=guy.uid).first()

    user_univ2 = dbSession.query(UserUniversity).filter_by(user_id=girl.uid).first()
    univ2 = user_univ2.university if user_univ2 else None
    user_school2 = dbSession.query(UserSchool).filter_by(user_id=girl.uid).first()
    school2 = user_school2.school if user_school2 else None
    user_career2 = dbSession.query(UserCareer).filter_by(user_id=girl.uid).first()

    # add comparison results for categorical features in the ouput vector
    # if both features specified the result is either 1 (equal) or 0 (not equal)
    # otherwise default value
    for x in categoricalFeatures:
        if x == 'city.id':
            x = "city"
        if x == 'country.id':
            x = "country"
        if x == 'relation':
            c = dbSession.query(Couple).filter_by(guy_id=guy.uid).first()
            vals.append(int(c.guy_relation == c.girl_relation)
                        if c and c.girl_relation and c.guy_relation else defCatVal)
            continue
        if x == 'site':
            vals.append(defCatVal)
            continue

        if x.startswith('university'):
            if user_univ1 and univ1 and user_univ2 and univ2:
                if x == 'university.id':
                    vals.append(univ1.university_id == univ2.university_id \
                                    if univ1.university_id and univ2.university_id else defCatVal)
                    continue
                if x == 'university.faculty':
                    vals.append(univ1.faculty_id == univ2.faculty_id \
                                    if univ1.faculty_id and univ2.faculty_id else defCatVal)
                    continue
                if x == 'university.faculty_name':
                    vals.append(univ1.faculty_name == univ2.faculty_name \
                                    if univ1.faculty_name and univ2.faculty_name else defCatVal)
                    continue
            else:
                vals.append(defCatVal)
                continue

        if x.startswith('school'):
            if user_school1 and school1 and user_school2 and school2:
                if x == 'school.type':
                    vals.append(school1.type == school2.type \
                                    if school1.type and school2.type else defCatVal)
                    continue
                if x == 'school.id':
                    vals.append(school1.school_id == school2.school_id \
                                    if school1.school_id and school2.school_id else defCatVal)
                    continue
            else:
                vals.append(defCatVal)
                continue
        if x == 'career.position':
            vals.append(user_career1.position == user_career2.position \
                            if (user_career1 and user_career1.position
                                and user_career2 and user_career2.position) else defCatVal)
            continue
        k = x.replace('.', '_')
        val1 = getattr(guy, k)
        val2 = getattr(girl, k)
        vals.append(int(val1 == val2) if val1 and val2 else defCatVal)
    return vals


# this function is used to map the difference of ages between to user into a value 0..1
def ageCombination(ageUserA, ageUserB):
    maxAge = max(ageUserA, ageUserB)  # value of the older user evaluated
    difference = abs(ageUserA - ageUserB)  # difference between the two users in absolute value
    lowerbound = (
                 maxAge // 2) + 7  # reading online we found that a good parter
                 # should be older than the half of yor age plus 7
    result = difference / (maxAge - lowerbound)  # we cast the difference into the interval
    # between maxAge and lowerbound
    # we want that when the difference is equals to zere we get the maximum result (1)
    # for this reason according to the algorithm to calculate result we need to do 1- the result
    return (1 - result)


# audios: 0 to 32767
# photos: 0 to 58762
# followers: 0 to 83388
# groups: 0 to 4999
# pages: 0 to 4126
# notes: 0 to 2936
# videos: 0 to 9998
def counter_category(counterA, counterB, name):
    '''
    Compares values of counters in terms of categories instead of value comparison
    :param counterA:
    :param counterB:
    :param name:
    :return: integer value 0 or 1
    '''
    if name in ["counters_audios", "counters_photos", "counters_followers"]:
        def getCat(a):
            if a < 100: return 0
            elif a < 1000: return 1
            elif a < 10000: return 2
            else: return 4
        return getCat(counterA) == getCat(counterB)
    elif name in ["counters_groups", "counters_pages", "counters_notes"]:
        def getCat(a):
            if a < 100: return 0
            elif a < 500: return 1
            elif a < 1000: return 2
            else: return 4
        return getCat(counterA) == getCat(counterB)
    elif name == "counters_videos":
        def getCat(a):
            if a < 100: return 0
            elif a < 1000: return 1
            elif a < 5000: return 2
            else: return 4
        return getCat(counterA) == getCat(counterB)
    else:
        print "counter %s not in list" % name
        return counterA == counterB


# The function passfilter get a vector and send back a True
# if at least 75% of it is fullfill with values different from None
def passfilter(x):
    c = 0  # counter
    for value in x:  # check all the elements in the array
        if value == defNumVal: c += 1  # if the element is not present
        # increase the counter

    if (c >= (len(x) // 1.5)):  # if the counter value is higher then the 25%
    # of the total lenght of the vector
        return False  # --> the result is FALSE
    else:
        return True  # otherwise the vector is not valid

if __name__ == '__main__':
    print model
    main()
