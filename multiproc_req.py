import multiprocessing as mp

def doWork(target, queue, return_dict):
    '''

    :param target:
    :param queue:
    :return:
    '''

    while True:
        job = queue.get()
        if job == None:
            break
        res = target(job)
        return_dict[job] = res
        queue.task_done()
    queue.task_done()

def doConcurrent(data, target):
   '''

   :param data:
   :param target:
   :return:
   '''
   # setup jobs
   nCPU = mp.cpu_count()
   print("nCPU % d" % nCPU)

   queue = mp.JoinableQueue()
   manager = mp.Manager()
   return_dict = manager.dict()
   nworkers = 100*nCPU
   for d in data:
      queue.put(d)
   for x in xrange(nworkers):
      queue.put(None)

   # run workers
   workers = []

   for i in range(nworkers):
      worker = mp.Process(target = doWork,
                          args = (target, queue, return_dict))
      workers.append(worker)
      worker.start()

   queue.join()
   return return_dict
