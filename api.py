'''
Contains all the methods to collect the data using API

'''
import requests
import io
import sys
import re
import os
import time
from db import dbSession, User

def collectToken(code, session):
   '''
   This method requests token for the given code
   :param code: code from api callback, see https://vk.com/dev/authcode_flow_user
   :param session: session from flask to store user_id and access_token
   :return:
   '''
   url = "https://oauth.vk.com/access_token?client_id=5652867&client_secret=4axX60HoTiP2f3nmp0lT&redirect_uri=http://54.218.70.60/code&code=%s"%code
   res = requests.get(url)
   print(res)
   res = res.json()
   access_token = res["access_token"]
   user_id = res['user_id']
   session['user_id'] = user_id
   session['access_token'] = access_token

class ScrapError(Exception):
   '''
   An error in vk response
   '''
   def __init__(self, response, message):
      self.response = response
      self.message = message
   def __str__(self):
      return str(self.response) +" " + str(self.message)

# fields which are requested from vk api
reqFields = ["uid",
               "sex","bdate","city","country","home_town",
               "universities","schools","last_seen", "site",
               "occupation","relation","personal",
               "wall_comments",
               "career","counters"]
# features which are extracted from the colleted fields
features = ["uid", "year", "zodiac", "career.position", "city.id", "counters.videos",
            "counters.photos","counters.audios","counters.notes","counters.groups",
            "counters.followers", "counters.pages", "country.id", "home_town",
            "occupation.id", "occupation.type"
            "personal.political", "personal.religion", "personal.people_main",
            "personal.life_main", "personal.smoking", "personal.alcohol",
            "relation", "site", "sex", "university.id", "university.faculty",
            "university.faculty_name", "school.type", "school.id",
            "wall_comments"]

# filename for first version
filename = os.path.join('data', 'friendsOfFriends2.csv')

fieldnames = ["pair_id"] + features

def getZodiac(day, month):
   '''
   Converts day and month into a string corresponding to zodiac sign
   :param day: integer day
   :param month: integer month
   :return: string
   '''
   zodiacs = ['capricorn', 'aquarius', 'pisces', 'aries',
              'taurus', 'gemini', 'cancer', 'leo',
              'virgo', 'libra', 'scorpio', 'sagittarius']
   days = [20, 19, 20, 20, 21, 21, 22, 22, 23, 23, 22, 21]


   if day > days[month-1]:
      return zodiacs[month%12]
   else:
      return zodiacs[month-1]

def process(pers):
   '''

   :param pers:
   :return:
   '''
   res = dict.fromkeys(features, '-1')
   if 'uid' in pers:
      res['uid'] = pers['uid']

   if 'bdate' in pers:
      parts = pers['bdate'].split('.')
      if len(parts) == 3:
         res['year'] = int(parts[2])
      res['zodiac'] = getZodiac(int(parts[0]), int(parts[1]))

   if 'career' in pers and len(pers['career']) > 0:
      last = pers['career'][-1]
      for x in ['position']:
         if x in last:
            res['career.'+x] = last[x]
   if 'city' in pers:
      res['city.id'] = pers['city']
   if 'country' in pers:
      res['country.id'] = pers['country']

   if 'counters' in pers:
      for x in ['videos', 'photos', 'audios', 'friends', 'notes', 'groups', 'followers', 'pages']:
         if x in pers['counters']:
            res['counters.'+x] = pers['counters'][x]
   if 'home_town' in pers:
      res['home_town'] = pers['home_town']
   if 'occupation' in pers:
      for x in ['id', 'type']:
         if x in pers['occupation']:
            res['occupation.'+x] = pers['occupation'][x]
   if 'personal' in pers:
      for x in ['political', 'religion', 'people_main', 'life_main', 'smoking', 'alcohol']:
         if x in pers['personal']:
            res['personal.'+x] = pers['personal'][x]
   if 'relation' in pers:
      res['relation'] = pers['relation']
   if 'sex' in pers:
      res['sex'] = pers['sex']
   if 'wall_comments' in pers:
      res['wall_comments'] = pers['wall_comments']

   if 'schools' in pers and len(pers['schools']) > 0:
      last = pers['schools'][-1]
      for x in ['id', 'type']:
         if x in last:
            res['school.'+x] = last[x]
   if 'photo_100' in pers:
      res['photo_100'] = pers

   if 'universities' in pers and len(pers['universities']) > 0:
      last = pers['universities'][-1]
      for x in ['id', 'faculty', 'faculty_name']:
         if x in last:
            res['university.'+x] = last[x]
   return res

def requestFields(id, session, fields = reqFields):
   res = requests.post("https://api.vk.com/method/users.get", params=
   {
      "user_ids":"%d"%id,
      "access_token":session['access_token'],
      "fields":','.join(fields)
   })
   res = res.json()
   if type(res) == dict and 'response' in res:
      res = res["response"]
   else:
      raise ScrapError(res, 'no response key')
   return res

def requestFieldsWithRepeat(id, session, fields = reqFields):
   try:
      res = requestFields(id, session, fields)
      return res
   except ScrapError as e:
      print('User_id %d'% id)
      print(e.message)
      if 'error' in e.response and 'error_msg'in e.response['error']:
         if e.response['error']['error_code']==6:
            time.sleep(10)
            res = requestFields(id, session, fields)
            return res
         else:
            raise e
      else:
         raise e

def requestFriends(id, session, fields):
   res = requests.get("https://api.vk.com/method/friends.get", params=
   {"user_id":id,
    "access_token": session['access_token'],
    "fields":','.join(fields)
   })
   res = res.json()
   if type(res) == dict and 'response' in res:
      friends = res["response"]
   else:
      raise ScrapError(res, 'no response key')
   return friends

def requestFriendsWithRepeat(id, session, fields):
   try:
      res = requestFriends(id, session, fields)
      return res
   except ScrapError as e:
      print('User_id %d'% id)
      print(e.message)
      if 'error' in e.response and 'error_msg'in e.response['error']:
         if e.response['error']['error_code']==6:
            time.sleep(10)
            res = requestFriends(id, session, fields)
            return res
         if e.response['error']['error_code']==18:
            return []
         else:
            print e
            raise e
      else:
         raise e


def getPairs(id, session, startPairId):

   friends = requestFriends(id, session, ['relation'])
   in_rel = []
   for u in friends:
      if 'relation'in u and u['relation']!=0:
         in_rel.append(u)

   pairs = [(x['relation_partner']['id'],x['user_id']) for x in in_rel if "relation_partner" in x]
   r = []
   i = startPairId

   for pair in pairs:
      for p in [pair[0], pair[1]]:
         res = requestFields(session, p)[0]
         res = process(res)
         res['pair_id'] = i
         r.append(res)
      i+=1
   return r, i

def makeUnique(pairs, pairsSet):
   notMet = []
   for i in xrange(0,len(pairs),2):
      id1 = pairs[i]['uid']
      id2 = pairs[i+1]['uid']
      if ((id1,id2) not in pairsSet and (id2, id1) not in pairsSet):
         notMet.append(pairs[i])
         notMet.append(pairs[i+1])
         pairsSet.add((id1,id2))
   return notMet

def dictToUnicode(d, fieldnames, sep=u','):
   res = u''
   j = 0
   length = len(fieldnames)
   for k in fieldnames:
      res += allToUnicode(d[k], k)
      if j!=length-1:
         res+=sep
      j+=1
   return res

def allToUnicode(l, fName):
   res = u''
   if type(l)==unicode and fName != 'photo_100':
      res += re.sub('(\s)+',' ',l.strip().lower().replace('\n','').replace(',',''))
   else:
      res += str(l).decode("utf-8")
   return res

def writeFieldnamesToFile():
   with io.open(filename, 'w', encoding='utf-8') as csvfile:
      csvfile.write(u','.join([x.decode("utf-8") for x in fieldnames])+u'\n')
      csvfile.flush()

def writeToFile(r):
   with io.open(filename, 'a', encoding='utf-8') as csvfile:
       for row in r:
         csvfile.write(dictToUnicode(row, fieldnames)+u'\n')
       csvfile.flush()

def main():
   pairsSet = set()
   startPairId = 0
   id = "3853209"
   access_token = "" #put something here
   session = {'user_id': id, 'access_token': access_token}
# to get token:
# https://oauth.vk.com/authorize?client_id=5652867&display=page&redirect_uri=http://127.0.0.1&response_type=token
#    users = dbSession.query(User).all()
#    for user in users:
#       res = requestFieldsWithRepeat(user.uid, session, ['photo_100'])
#       user.photo_100 = res[0]['photo_100']
#       dbSession.add(user)
#       dbSession.commit()
   # r,startPairId = getPairs(id, session, startPairId)
   #
   # r = makeUnique(r,pairsSet)
   # writeFieldnamesToFile()
   # writeToFile(r)
   # for friend in friends:
   #    try:
   #       pairs, startPairId = getPairs(friend['uid'], session, startPairId)
   #       pairs = makeUnique(pairs, pairsSet)
   #       writeToFile(pairs)
   #    except:
   #       print "Unexpected error:", sys.exc_info()[0]


   print("file created %s"%filename)
if __name__ ==  '__main__':
   main()


