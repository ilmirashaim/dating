'''
Contains methods to turn the API responses into objects which can be persisted in the database
'''
from sqlalchemy.exc import SQLAlchemyError
from db import User, University, UserCareer, School, UserSchool, UserUniversity, Couple
from db import get_or_create
from api import requestFriendsWithRepeat
from api import requestFieldsWithRepeat
from api import getZodiac
import sys
import time
import logging
import traceback
from db import dbSession

# fields collected by api
fields = ["uid", "last_name", "first_name",
          "city", "country", "home_town",
          "occupation", "career", "photo_100",
          "personal", "deactivated", "verified",
          "relation", "universities", "schools", "sex", "bdate", "counters",
          "wall_comments", "books", "movies", "activities", "interests",
          "games", "music", "tv", "last_seen"]
# fields which are left the same in the database
fieldsNoChange = ["last_name", "first_name","home_town","books", "movies", "activities", "interests",
          "games", "music", "tv", "photo_100", "verified", "wall_comments", "city", "country"]
# fields which has subfields and are called as field_subfield in the model
fieldsSub = {'counters':['videos','photos','audios', 'notes', 'groups', 'followers', 'pages'],
             'personal':['political', 'religion', 'people_main', 'life_main', 'smoking', 'alcohol'],
             'occupation': ['id', 'type']}


def checkKey(k, obj):
   return obj[k] if k in obj else None

def processStr(str):
   if str is None:
      return str
   return str.strip().lower()

def setUniversities(x, user, session):
   if 'universities' in x:
      for univ in x['universities']:
         university, created = get_or_create(session, University,
                                    city = checkKey('city', univ),
                                    country = checkKey('country', univ),
                                    university_id = checkKey('id', univ),
                                    name = processStr(checkKey('name', univ)),
                                    faculty_id = checkKey('faculty', univ),
                                    faculty_name = processStr(checkKey('faculty_name', univ)))
         user_university = get_or_create(session, UserUniversity, user = user, university = university)

def setSchools(x, user, session):
   if 'schools' in x:
      for school in x['schools']:
         school, created = get_or_create(session, School,
                                    city = checkKey('city', school),
                                    country = checkKey('country', school),
                                    school_id = checkKey('id', school),
                                    type = checkKey('type', school),
                                    name = processStr(checkKey('name', school)))
         user_school = get_or_create(session, UserSchool, user = user, school = school)

def setCareers(x, user, session):
   if 'career' in x:
      for career in x['career']:
         userCareer = UserCareer(city = checkKey('city_id', career),
                                    country = checkKey('country_id', career),
                                    position = processStr(checkKey('position', career)),
                                    user = user)
         session.add(userCareer)

def convertSecondsToHoursDays(t):
   t = int(t)
   hours = (t/3600) %24
   days = (t/3600) /24
   return hours, days


def collectAndCreate(uid, session, httpSess):
   now = time.time()
   res = requestFieldsWithRepeat(uid, httpSess, fields)
   x = res[0]
   user, created = get_or_create(session, User, uid = x['uid'])
   if not created:
      return x, user, created

   for field in x:
      if (type(x[field]) == unicode or type(x[field]) == str) and field != 'photo_100':
         x[field] = x[field].strip().lower()
      if type(x[field]) == dict:
         for sub in x[field]:
            if type(x[field][sub]) == unicode or type(x[field][sub]) == str:
               x[field][sub] = x[field][sub].strip().lower()


   for field in fieldsNoChange:
      if field in x:
         setattr(user, field, x[field])

   user.sex = bool(x["sex"] - 1)

   if 'bdate' in x:
      parts = x['bdate'].split('.')
      if len(parts) == 3:
         user.bdate = '-'.join([parts[2], parts[1], parts[0]])
         user.zodiac = getZodiac(int(parts[0]), int(parts[1]))
      elif len(parts) == 1:
         user.bdate = '-'.join([parts[0], '0', '0'])
   if 'last_seen' in x:
      if 'time' in x['last_seen']:
         hours, days = convertSecondsToHoursDays(now- x['last_seen']['time'])
         user.last_seen_hours = hours
         user.last_seen_days = days

   for field in fieldsSub:
      if field in x:
         xf = x[field]
         for sub in fieldsSub[field]:
            if sub in xf:
               setattr(user, field+'_'+sub, xf[sub])

   session.add(user)

   setUniversities(x, user, session)
   setSchools(x, user, session)
   setCareers(x, user, session)

   session.commit()
   return x, user, created

def collectRelation(x, user, session):
   partner = None
   if 'relation' in x and x['relation'] != '1':
      if 'relation_partner' in x and 'id' in x['relation_partner']:
         if user.sex == 1: #if guy
            couple = session.query(Couple).filter_by(girl_id = x['relation_partner']['id']).first()
            if couple is None:
               couple = Couple()
               partner = x['relation_partner']['id']
            couple.guy = user
            couple.guy_relation = x['relation']
         else:
            couple = session.query(Couple).filter_by(guy_id = x['relation_partner']['id']).first()
            if couple is None:
               couple = Couple()
               partner = x['relation_partner']['id']
            couple.girl = user
            couple.girl_relation = x['relation']
         session.add(couple)
         session.commit()
   return partner

def setPartner(x_p, user_p, user, session):
   if user_p.sex == 1: #if guy
      couple = session.query(Couple).filter_by(girl_id = user.uid).first()
      if couple is None:
         return
      couple.guy = user_p
      couple.guy_relation = x_p['relation'] if 'relation' in x_p else None
   else:
      couple = session.query(Couple).filter_by(guy_id = user.uid).first()
      if couple is None:
         return
      couple.girl = user_p
      couple.girl_relation = x_p['relation'] if 'relation' in x_p else None
   session.add(couple)
   session.commit()

def collectAndCreateFriends(friends, session, httpSess):
   uids = [f['uid'] for f in friends]
   users = []
   for uid in uids:
      try:
         x, user, created = collectAndCreate(uid, session, httpSess)
         users.append(user)
         if not created:
            continue
         partner = collectRelation(x, user, session)
         if partner is not None:
            x_p, user_p, created = collectAndCreate(partner, session, httpSess)
            setPartner(x_p, user_p, user, session)
            users.append(user_p)
      except SQLAlchemyError as e:
         print e.message
         logging.error(traceback.format_exc())
         session.rollback()
      except Exception as e:
         logging.error(traceback.format_exc())
         print('error while collecting info from user %d'% uid)
         print "Unexpected error:", sys.exc_info()[0]
         session.rollback()

   return users


def createUserWithFriends(uid, httpSess):
   x, user, created = collectAndCreate(uid, dbSession, httpSess)
   friends = requestFriendsWithRepeat(uid, httpSess, ['uid'])
   collectAndCreateFriends(friends, httpSess, dbSession)
   return user

