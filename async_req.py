import grequests

def do_async(tasks):

   # A list to hold our things to do via async
   async_list = []

   for t in tasks:
       action_item = grequests.post(t['url'], data = t['params'])

       # Add the task to our list of things to do via async
       async_list.append(action_item)

   # Do our list of things to do via async
   res = grequests.map(async_list)
   return res
