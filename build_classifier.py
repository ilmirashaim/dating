from math import fabs
from random import shuffle
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.model_selection import cross_val_score, GridSearchCV
import sklearn.metrics as metrics
from sklearn.externals import joblib
from sklearn.decomposition import PCA
from transform import Transformation
import os
from database_setup import Couple, dbSession, UserUniversity, UserSchool, UserCareer

modelFilename = os.path.join('model', 'rfc_100_label.pkl')
# modelFilename = os.path.join('model', 'rfc_100.pkl')
# modelFilename = os.path.join('model', 'svc.pkl')
pcaFilename = os.path.join('model', 'pca.pkl')
model = 'rf'
# model = 'svc'

# oneHot = True
oneHot = False
need_pca = False

def permuteData(X, y):
   ind = range(len(X))
   shuffle(ind)

   X_new = ([X[i] for i in ind])
   y_new = ([y[i] for i in ind])
   return X_new, y_new

categoricalFeatures = [
               "zodiac",
               "city.id",
               "country.id",
                #"home_town",
               "occupation.id",
               "occupation.type",
               "personal.political",
               "personal.religion",
               "personal.people_main",
               "personal.life_main",
               "personal.smoking",
               "personal.alcohol",
               #"site",
               #"university.id",
               "university.faculty",
               #"university.faculty_name",
               "school.type", "school.id"]
               # "career.position"]
numericFeatures = ["sex", "year",
               #"counters.videos",
               "counters.photos",
               "counters.audios",#"counters.notes",
               "counters.groups",
               "counters.followers",
               "counters.pages"]#, "wall_comments"]

def generateSamples(notTransformed, transformed):
   X = []
   Y = []
   length = len(transformed)
   for i in xrange(0, 2*(length/2), 2):
      if i+3 < length:
         j = i+3
      else:
         j = i-3

      x = notTransformed[i] + transformed[i] + notTransformed[i+1] + transformed[i+1]
      X.append(x)
      Y.append(1)
      x = notTransformed[i+1] + transformed[i+1] + notTransformed[i] + transformed[i]
      X.append(x)
      Y.append(1)

      x = notTransformed[i] + transformed[i] + notTransformed[j] + transformed[j]
      X.append(x)
      Y.append(0)
      x = notTransformed[i+1] + transformed[i+1] + notTransformed[j-1] + transformed[j-1]
      X.append(x)
      Y.append(0)
   return X, Y

def couplesToData(couples):
   from dbToModel import process
   forTransform = []
   notTransformed = []
   for couple in couples:
      if not couple.girl or not couple.guy:
         # print(couple.id)
         continue
      guy=process(couple.guy, dbSession)
      notTransformed.append(guy[0])
      forTransform.append(guy[1])

      girl=process(couple.girl, dbSession)
      notTransformed.append(girl[0])
      forTransform.append(girl[1])
   return forTransform, notTransformed

def main():

   couples = dbSession.query(Couple).all()
   forTransform, notTransformed = couplesToData(couples[400:])
   testForTransform, testNotTransformed = couplesToData(couples[:400])

   encoder = Transformation(oneHot, categoricalFeatures)
   transformed = encoder.fit(forTransform)
   testTransformed = encoder.transform(testForTransform)

   X, Y = generateSamples(notTransformed, transformed)
   print ('number of features = %d' % len(X[0]))

   Xtest, Ytest = generateSamples(testNotTransformed, testTransformed)

   if need_pca:
      pca = PCA(n_components=100)
      pca.fit(X)
      X = pca.transform(X)
      Xtest = pca.transform(Xtest)
      joblib.dump(pca, pcaFilename)

   if model == 'svc':
      param_grid = {'C': [1,2,3],
              'gamma': [0.01, 0.001, 0.0001]}
      clf = SVC()
   else:
      param_grid = {'n_estimators': [10,20,100],
              'criterion': ['gini', 'entropy'], 'max_features':['auto','log2']}
      clf = RandomForestClassifier()

   gridSearchClf = GridSearchCV(clf, param_grid, scoring='accuracy', cv=3)
   gridSearchClf.fit(X, Y)

   print("\nAccuracy %f" % ( gridSearchClf.best_score_))
   print(gridSearchClf.best_params_)
   if model == 'svc':
      C = gridSearchClf.best_params_['C']
      gamma = gridSearchClf.best_params_['gamma']
      clf = SVC(gamma=gamma, C=C)
   else:
      n_estimators = gridSearchClf.best_params_['n_estimators']
      criterion = gridSearchClf.best_params_['criterion']
      max_features = gridSearchClf.best_params_['max_features']

      clf = RandomForestClassifier(n_estimators = n_estimators,
                                   max_features=max_features,
                                   criterion=criterion)

   print('Training data, cross validation')
   for j in xrange(1):
      # X, Y = permuteData(X, Y)
      scores = ['accuracy','precision', 'recall', 'f1_weighted', 'roc_auc']
      for score in scores:
         print(score)
         r = cross_val_score(clf, X, Y, cv=3, scoring=score)
         print(r)
   clf.fit(X,Y)

   joblib.dump(clf, modelFilename)

   print('\n\nTest data')
   testPred = clf.predict(Xtest)
   print"confusion matrix\n %s" % str(metrics.confusion_matrix(Ytest, testPred))
   print("accuracy %f" % metrics.accuracy_score(Ytest, testPred))
   print("recall %f" % metrics.recall_score(Ytest, testPred))
   print("precision %f" % metrics.precision_score(Ytest, testPred))
   print("roc auc score %f" % metrics.roc_auc_score(Ytest, testPred))


defNumVal = -1
defCatVal = -1

def process(user, session):
   numValues = []
   for x in numericFeatures:
      if x == 'sex':
         numValues.append(float(user.sex+1))
         continue
      if x == 'year':
         year = user.bdate.year if user.bdate else defNumVal
         numValues.append(float(year))
         continue
      x = x.replace('.','_')
      if x == 'counters_videos':
         numValues.append(defNumVal)
         continue
      val = getattr(user, x)
      numValues.append(float(val if val else defNumVal))

   categoricalValues = {}
   user_univ = session.query(UserUniversity).filter_by(user_id = user.uid).first()
   univ = user_univ.university if user_univ else None
   user_school = session.query(UserSchool).filter_by(user_id = user.uid).first()
   school = user_school.school if user_school else None
   user_career = session.query(UserCareer).filter_by(user_id = user.uid).first()
   for x in categoricalFeatures:
      if x == 'city.id':
         categoricalValues[x] = user.city if user.city else defCatVal
         continue
      if x == 'country.id':
         categoricalValues[x] = user.country if user.country else defCatVal
         continue
      if x == 'relation':
         if user.sex == '1':
            couple = session.query(Couple).filter_by(guy_id = user.uid).first()
            relation = couple.guy_relation if (couple and couple.guy_relation) else defCatVal
         else:
            couple = session.query(Couple).filter_by(guy_id = user.uid).first()
            relation = couple.girl_relation if (couple and couple.girl_relation) else defCatVal
         categoricalValues[x] = relation
         continue
      if x == 'site':
         categoricalValues[x] = defCatVal
         continue
      if x.startswith('university'):
         if user_univ and univ:
            if x == 'university.id':
               categoricalValues[x] = univ.university_id if univ.university_id else defCatVal
               continue
            if x == 'university.faculty':
               categoricalValues[x] = univ.faculty_id if univ.faculty_id else defCatVal
               continue
            if x == 'university.faculty_name':
               categoricalValues[x] = univ.faculty_name if univ.faculty_name else defCatVal
               continue
         else:
            categoricalValues[x] = defCatVal
            continue

      if x.startswith('school'):
         if user_school and school:
            if x == 'school.type':
               categoricalValues[x] = school.type if school.type else defCatVal
               continue
            if x == 'school.id':
               categoricalValues[x] = school.school_id if school.school_id else defCatVal
               continue
         else:
            categoricalValues[x] = defCatVal
            continue
      if x == 'career.position':
         categoricalValues[x] = user_career.position if (user_career and user_career.position) else defCatVal
         continue
      k = x.replace('.','_')
      val = getattr(user, k)
      categoricalValues[x] = val if val else defCatVal
   return numValues, categoricalValues





if __name__ ==  '__main__':
   main()