from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction import DictVectorizer
from sklearn.externals import joblib
import os

class Transformation:
   def __init__(self, oneHot, features = None, defaultValue = -1):
      self.defaultValue = defaultValue
      self.oneHot = oneHot
      self.encodersPath = os.path.join('model','encoders')
      if oneHot:
         self.encoderFilename = os.path.join(self.encodersPath, 'onehotencoder.pkl')
         self.encoder = OneHotTransform(self.encoderFilename, self.defaultValue)
      else:
         self.encoderFilename = os.path.join(self.encodersPath, '%s_labelencoder.pkl')
         self.encoder = LabelTransform(self.encoderFilename, features, self.defaultValue)

   def fit(self, forTransform):
      transformed = self.encoder.fit(forTransform)
      return transformed

   def transform(self, testForTransform):
      testTransformed = self.encoder.transform(testForTransform)
      return testTransformed


class OneHotTransform():
   def __init__(self, filename, defaultValue):
      self.filename = filename
      self.vec = None
      self.defaultValue = defaultValue

   def fit(self, forTransform):
      self.vec = DictVectorizer()
      transformed = self.vec.fit_transform(forTransform).toarray().tolist()
      joblib.dump(self.vec, self.filename)
      return transformed

   def transform(self, forTransform):
      if self.vec is None:
         self.vec = joblib.load(self.filename)
      return self.vec.transform(forTransform).toarray().tolist()


class LabelTransform():
   def __init__(self, filename, features, defaultValue):
      self.filename = filename
      self.encoders = []
      self.features = features
      self.defaultValue = defaultValue

   def clean(self, val):
      if type(val) not in [str, unicode]:
         return val
      val = val.strip().lower().replace('\n','').replace(',','')
      if type(val) == unicode:
         return val.encode('utf-8')
      return val

   def fit(self, forTransform):
      transformed = [[] for i in xrange(len(forTransform))]

      for f in self.features:
         encoder = LabelEncoder()
         self.encoders.append(encoder)
         featData = [self.clean(x[f]) for x in forTransform]
         feat = encoder.fit_transform(featData + [self.defaultValue])
         joblib.dump(encoder, self.filename%f)

         for i in xrange(len(forTransform)):
            transformed[i].append(feat[i])

      return transformed

   def transform(self, forTransform):
      transformed = [[] for i in xrange(len(forTransform))]

      if not self.encoders:
         for f in self.features:
            encoder = joblib.load(self.filename%f)
            self.encoders.append(encoder)

      for j in xrange(len(self.features)):
         featData = [self.clean(x[self.features[j]]) for x in forTransform]
         featData = [val if val in self.encoders[j].classes_ else self.defaultValue
                     for val in featData]
         feat = self.encoders[j].transform(featData)

         for i in xrange(len(forTransform)):
            transformed[i].append(feat[i])
      return transformed